(ns gr.test.endpoints)

(use '[gr.routes])

(endpoints {:uri "/" :request-method :get})
(endpoints {:uri "/about" :request-method :get})

(endpoints {:uri "/api/auth" :request-method :post})
(endpoints {:uri "/api/create-user" :request-method :post})
(endpoints {:uri "/not-exist-endpoints" :request-method :post})

;; fix static resources
