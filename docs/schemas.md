
### Design Schemas

```clj

------------------------
;; identity
:person/first-name
:person/last-name
:person/born-year
:person/phone-number
:person/gender
:person/main-address

:company/name
:company/main-addreses
:company/phone-number


:auth/roles
:auth/permissions #{:create-services
                    :create-vendor
                    :create-customer}

:auth/created-key-token
:auth/activation-time
:auth/activation-key
:auth/email-activated?
:auth/email
:auth/password

:customer/identity
:customer/active?
:customer/created-at
:customer/auth
:customer/account

:vendor/identity
:vendor/auth
:vendor/active?
:vendor/created-at
:vendor/account
:vendor/ratings #{:rate/user :rate/value}
:vendor/quota-limit
:vendor/assigned-location #{}


[:staff/identity
 :staff/auth
 :staff/account
 :staff/active?
 :staff/created-at

 :admin/identity
 :admin/auth
 :admin/account
 :admin/active?
 :admin/created-at]

:account/type
:account/transactions #{:transaction/type :payment :credit :debit :charge
                        :transaction/amont}
:account/current-balance

:services/name
:service/document-type
:service/prices

:orders
:orders/id
:orders/customer
:orders/status
:orders/services
:orders/coordinates #{geolat/lon posisitions}
:orders/provinces
:orders/city
:orders/book-date
:orders/created-date


:payments
:payment/account
:payment/meta-data :alot-of :include-orders
:payment/amounts
:payment/methods

:assignment
:assignment/vendor
:assignment/order
:assignment/date
:assignment/book-date
:assignment/status :completed :inprogress

:geo/id
:geo/lat
:geo/lon
:geo/name

:addresses/name
:country/name
```
